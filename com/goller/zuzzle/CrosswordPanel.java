package goller.zuzzle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class CrosswordPanel extends JPanel implements MouseListener, KeyListener {
    private static final long serialVersionUID = 1L;
    CrosswordPuzzle puzzle;
    Square currentSelection = new Square();
    public int orientation = 0; // looking across or down

    public CrosswordPanel(int width, int height) {
        setLayout(new GridLayout(width, height));
        puzzle = new CrosswordPuzzle(width, height);
        puzzle.add(this, this, this); // hahah pretty ambiguous... a key listener, a mouse listener, and the container...in reverse order :)
        select(puzzle.nextWhite(0, 0));

    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            select((Square) e.getComponent());
        } else if (e.getButton() == MouseEvent.BUTTON3) {
            ((Square) e.getComponent()).toggleBlack();
            puzzle.addNumbers();
            select(currentSelection);
        }
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            ++orientation;
            orientation %= 2;
            select(currentSelection);
        }
        if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            currentSelection.input("");
            puzzle.retreat(currentSelection, orientation, this);
        } else if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            currentSelection.input("");
            puzzle.advance(currentSelection, orientation, this);
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            puzzle.retreat(currentSelection, 0, this);
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            puzzle.advance(currentSelection, 0, this);
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            puzzle.advance(currentSelection, 1, this);
        } else if (e.getKeyCode() == KeyEvent.VK_UP) {
            puzzle.retreat(currentSelection, 1, this);
        } else if (e.getKeyCode() == KeyEvent.VK_TAB) {
            if (e.isShiftDown())
                select(puzzle.previousClue(currentSelection.gridX, currentSelection.gridY));
            else
                select(puzzle.nextClue(currentSelection.gridX, currentSelection.gridY));
        }
    }

    public void keyReleased(KeyEvent arg0) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if (e.getKeyChar() == KeyEvent.VK_SPACE || e.getKeyChar() == KeyEvent.VK_BACK_SPACE || e.getKeyChar() == KeyEvent.VK_DELETE || e.getKeyChar() == KeyEvent.VK_LEFT || e.getKeyChar() == KeyEvent.VK_RIGHT ||
                e.getKeyChar() == KeyEvent.VK_DOWN || e.getKeyChar() == KeyEvent.VK_UP || e.getKeyChar() == KeyEvent.VK_TAB) {
            return;
        }

        currentSelection.input("" + e.getKeyChar());
        puzzle.advance(currentSelection, orientation, this);
    }

    public void select(Square s) {
        if (!s.isBlack()) {
            puzzle.select(s, orientation);
            currentSelection = s;
            s.requestFocusInWindow();
        }
    }
}
