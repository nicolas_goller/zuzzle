package goller.zuzzle;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class Controller extends JFrame {
    private static final long serialVersionUID = 1L;
    public static Font mainFont, numberFont;
    public static Border blackline, selectedMajor, selectedMinor;
    public static Color borderColor, selectedColor, selectedBackground, normalBackground, darkBackground, selectedCompatriots;

    public Controller() {
        super("Zuzzle");
        setResizable(true);
        setUndecorated(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(800, 800);
        //setting static variables
        borderColor = Color.black;
        normalBackground = Color.white;
        darkBackground = Color.black;
        selectedColor = new Color(3, 89, 163); // dark blue
        selectedBackground = new Color(143, 199, 247); // light blue
        selectedCompatriots = new Color(109, 163, 204); // lighter blue
        mainFont = new Font("Arial", Font.BOLD, 14);
        numberFont = new Font("Arial", Font.PLAIN, 10);
        blackline = BorderFactory.createLineBorder(borderColor);
        selectedMajor = BorderFactory.createLineBorder(selectedColor);
        selectedMinor = BorderFactory.createLineBorder(selectedColor.brighter());
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        setVisible(true);

        StartPanel mainMenu = new StartPanel();

        add(mainMenu);
        revalidate();
    }

    public static void main(String args[]) {
        new Controller();
    }
}
