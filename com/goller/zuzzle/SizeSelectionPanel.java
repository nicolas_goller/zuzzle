package goller.zuzzle;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Hashtable;

public class SizeSelectionPanel extends JPanel implements ChangeListener, PropertyChangeListener, ActionListener {
    private static final long serialVersionUID = 1L;
    private static final int maxSize = 80;
    private static final int minSize = 1;
    private static final int defaultSize = 17;
    JFormattedTextField leftSize, rightSize;
    JSlider sizeSelector;
    int width, height;
    JLabel by, title;
    JButton ok;
    StartPanel parent;

    public SizeSelectionPanel(StartPanel parent) {
        this.parent = parent;
        setLayout(new GridBagLayout());
        sizeSelector = new JSlider(JSlider.VERTICAL, minSize, maxSize, defaultSize);
        sizeSelector.addChangeListener(this);
        sizeSelector.setMajorTickSpacing(10);
        sizeSelector.setPaintTicks(true);

        leftSize = new JFormattedTextField(defaultSize);
        rightSize = new JFormattedTextField(defaultSize);
        leftSize.addPropertyChangeListener(this);
        rightSize.addPropertyChangeListener(this);
        setupLabels();
        loadLabels();

        ok = new JButton("Go");
        ok.addActionListener(this);
        title = new JLabel("Select Size");
        placeComponents();
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        JSlider source = (JSlider) e.getSource();
        if (!source.getValueIsAdjusting()) {
            int size = (int) source.getValue();
            width = size;
            height = size;
            setLabels(size);
        }
    }

    public void setLabels(int size) {
        leftSize.setValue(size);
        rightSize.setValue(size);
    }

    @Override
    public void propertyChange(PropertyChangeEvent e) {
        Object source = e.getSource();
        if (source == leftSize) {
            width = (int) (leftSize.getValue());
            sizeSelector.setValue(width);
        } else if (source == rightSize) {
            height = (int) (rightSize.getValue());
        }
    }

    public void setupLabels() {
        leftSize.setValue(new Integer(defaultSize));
        leftSize.setColumns(3);
        leftSize.addPropertyChangeListener("value", this);
        rightSize.setValue(new Integer(defaultSize));
        rightSize.setColumns(3);
        rightSize.addPropertyChangeListener("value", this);

        by = new JLabel("by");
    }

    public void loadLabels() {
        Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
        //populate table
        //labelTable.put(new Integer(1), new JLabel("Once"));
        //labelTable.put(new Integer(4), new JLabel("Petite"));
        labelTable.put(new Integer(7), new JLabel("Micro"));
        //labelTable.put(new Integer(13), new JLabel("Small"));
        labelTable.put(new Integer(17), new JLabel("Medium"));
        labelTable.put(new Integer(30), new JLabel("Formidable"));
        labelTable.put(new Integer(50), new JLabel("Grande"));
        labelTable.put(new Integer(80), new JLabel("Titanic"));
        sizeSelector.setLabelTable(labelTable);
        sizeSelector.setPaintLabels(true);
    }

    public void placeComponents() {

        // add j slider
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.VERTICAL;
        c.gridx = 0;
        c.gridy = 2;
        c.ipadx = 20;
        c.gridheight = 3;
        c.gridwidth = 1;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        add(sizeSelector, c);

        // add label
        c = new GridBagConstraints();
        c.gridx = 2;
        c.gridy = 0;
        c.ipady = 20;
        //c.gridheight = 1;
        c.gridwidth = 1;
        add(title, c);

		/*JSeparator line = new JSeparator(SwingConstants.HORIZONTAL);
		line.setPreferredSize(new Dimension(400,5));
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 1;
		add(line,c);		*/

        // add first formatted text field
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 3;
        c.gridheight = 2;
        c.gridwidth = 1;
        add(leftSize, c);

        // add JLabel
        c = new GridBagConstraints();
        c.gridx = 2;
        c.gridy = 3;
        c.gridheight = 2;
        c.gridwidth = 1;
        add(by, c);

        // add right formatted text field
        c = new GridBagConstraints();
        c.gridx = 3;
        c.gridy = 3;
        c.gridheight = 2;
        c.gridwidth = 1;
        add(rightSize, c);

        // add button
        c = new GridBagConstraints();
        c.gridx = 2;
        c.gridy = 5;
        c.gridheight = 1;
        c.gridwidth = 1;
        add(ok, c);
        repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (ok == (JButton) e.getSource())
            startBuilding();
    }

    public void startBuilding() {
        parent.startBuilding(width, height);
    }
}
	