package goller.zuzzle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

public class StartPanel extends JPanel implements ActionListener, ComponentListener {

    private static final long serialVersionUID = 1L;

    static String title = "Puzzlemaster";
    JPanel buildPanel, playPanel;
    SizeSelectionPanel sizeSelector;
    CrosswordPanel crosswordPanel;
    private boolean listening;

    public StartPanel() {
        setLayout(new FlowLayout());

        //panels
        buildPanel = new JPanel();
        playPanel = new JPanel();

        buildPanel.setLayout(new BoxLayout(buildPanel, BoxLayout.Y_AXIS));
        playPanel.setLayout(new BoxLayout(playPanel, BoxLayout.Y_AXIS));

        //images
        ImageIcon buildImage = new ImageIcon("com/resources/build.png");
        ImageIcon playImage = new ImageIcon("com/resources/play.png");

        JLabel buildImageLabel = new JLabel(buildImage);
        JLabel playImageLabel = new JLabel(playImage);

        buildImageLabel.setAlignmentX(CENTER_ALIGNMENT);
        playImageLabel.setAlignmentX(CENTER_ALIGNMENT);

        buildPanel.add(buildImageLabel);
        playPanel.add(playImageLabel);

        //set up buttons
        JButton playButton, buildButton;
        playButton = new JButton("Play");
        buildButton = new JButton("Build");

        playButton.setActionCommand("play");
        buildButton.setActionCommand("build");

        playButton.addActionListener(this);
        buildButton.addActionListener(this);

        playButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        buildButton.setAlignmentX(Component.CENTER_ALIGNMENT);

        playButton.setToolTipText("Solve a Crossword");
        buildButton.setToolTipText("Make your own puzzle");

        buildPanel.add(buildButton);
        playPanel.add(playButton);
        add(playPanel);
        add(buildPanel);
        setVisible(true);

        addComponentListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "play":
                break;
            case "build":
                int result = showNewOpenDialog();

                switch (result) {
                    case 0: // clicked new
                        clearPanel();
                        addSizeSelectionPanel();
                        break;
                    case 1:
                        break; // clicked open
                }
                break;
        }
    }

    private void addSizeSelectionPanel() {
        sizeSelector = new SizeSelectionPanel(this);
        add(sizeSelector);
        revalidate();
    }

    public int showNewOpenDialog() {
        Object[] options = {"New", "Open"};
        return JOptionPane.showOptionDialog(this, "Would you like to open a new project or an existing project?",
                "New or Existing Project",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,     //do not use a custom Icon
                options,  //the titles of buttons
                options[0]); //default button title
    }

    private void clearPanel() {
        remove(playPanel);
        remove(buildPanel);
        repaint();
    }

    public void startBuilding(int width, int height) {
        removeAll();
        setLayout(new BorderLayout());
        crosswordPanel = new CrosswordPanel(width, height);
        JScrollPane scroller = new JScrollPane(crosswordPanel);
        add(scroller, BorderLayout.CENTER);
        revalidate();
        // this line has to be after the adding to the scroll pane mind you!
        crosswordPanel.select(crosswordPanel.puzzle.nextWhite(0, 0));
        listening = true;
    }

    public void componentHidden(ComponentEvent arg0) {
    }

    public void componentMoved(ComponentEvent arg0) {
    }

    public void componentShown(ComponentEvent arg0) {
    }

    public void componentResized(ComponentEvent arg0) {
        if (listening) {
            crosswordPanel.puzzle.redraw();
        }
    }
}
