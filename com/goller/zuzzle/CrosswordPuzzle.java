package goller.zuzzle;

import java.awt.*;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Iterator;

public class CrosswordPuzzle {
    private Square[][] puzzle;
    private String[] cluesAcross;
    private String[] cluesDown;
    private ArrayList<Square> squaresWithNumbers;
    private int width, height;
    private ArrayList<Square> minorSelected;

    public CrosswordPuzzle(int width, int height) {
        this.width = width;
        this.height = height;
        puzzle = new Square[height][width];
        minorSelected = new ArrayList<Square>();
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                puzzle[j][i] = new Square(i, j);
            }
        }

        addNumbers();
    }

    public CrosswordPuzzle(String filename) {
        // read in crossword file :)
    }

    public void add(Container c, MouseListener m, KeyListener k) {
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                puzzle[j][i].addMouseListener(m);
                puzzle[j][i].addKeyListener(k);
                c.add(puzzle[j][i]);
            }
        }
    }

    public void redraw() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                puzzle[i][j].redrawString();
            }
        }
    }

    public void addNumbers() {
        int number = 0;

        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                if (!puzzle[j][i].isBlack()) {
                    if (i == 0 || j == 0) {
                        puzzle[j][i].number = ++number;
                    } else if (puzzle[j - 1][i].isBlack() || puzzle[j][i - 1].isBlack()) {
                        puzzle[j][i].number = ++number;
                    } else {
                        puzzle[j][i].number = 0;
                    }
                }
                puzzle[j][i].repaint();
            }
        }
    }

    // go to next white skipping over blacks
    public void advance(Square currentSelection, int orientation, CrosswordPanel parent) {
        advance(currentSelection, orientation, parent, false);
    }

    public void advance(Square currentSelection, int orientation, CrosswordPanel parent, boolean nextClue) {
        boolean onWhite = false, passingBlack = false, passedBlack = false;
        Square safeSquare = currentSelection;

        if (orientation == 0) {
            while (currentSelection.gridX != width - 1 && (!onWhite || !passedBlack)) {
                if (currentSelection.gridX != width - 1) {
                    currentSelection = puzzle[currentSelection.gridY][currentSelection.gridX + 1];
                } else {
                    if (!currentSelection.isBlack())
                        passedBlack = true;
                }

                if (!currentSelection.isBlack()) {
                    if (passingBlack) {
                        passedBlack = true;
                    }
                    onWhite = true;
                    if (!nextClue) {
                        passedBlack = true;
                    }
                    safeSquare = currentSelection;
                } else if (currentSelection.isBlack()) {
                    passingBlack = true;
                }
            }
        } else if (orientation == 1) {
            while ((currentSelection.gridY != height - 1) && (!onWhite || !passedBlack)) {
                if (currentSelection.gridY != height - 1) {
                    currentSelection = puzzle[currentSelection.gridY + 1][currentSelection.gridX];
                } else {
                    if (!currentSelection.isBlack())
                        passedBlack = true;
                }

                if (!currentSelection.isBlack()) {
                    if (passingBlack) {
                        passedBlack = true;
                    }
                    onWhite = true;
                    if (!nextClue) {
                        passedBlack = true;
                    }
                    safeSquare = currentSelection;
                } else if (currentSelection.isBlack()) {
                    passingBlack = true;
                }

            }
        }

        currentSelection = safeSquare;
        parent.select(currentSelection);
    }

    // go to previous white skipping over blacks
    public void retreat(Square currentSelection, int orientation, CrosswordPanel parent) {
        retreat(currentSelection, orientation, parent, false);
    }

    public void retreat(Square currentSelection, int orientation, CrosswordPanel parent, boolean nextClue) {
        boolean onWhite = false, passingBlack = false, passedBlack = false;
        Square safeSquare = currentSelection;

        if (orientation == 0) {
            while (currentSelection.gridX != 0 && (!onWhite || !passedBlack)) {

                if (currentSelection.gridX != 0) {
                    currentSelection = puzzle[currentSelection.gridY][currentSelection.gridX - 1];
                } else {
                    if (!currentSelection.isBlack())
                        passedBlack = true;
                }

                if (!currentSelection.isBlack()) {
                    onWhite = true;
                    if (!nextClue) {
                        passedBlack = true;
                    }
                    safeSquare = currentSelection;
                } else if (currentSelection.isBlack()) {
                    passingBlack = true;
                }
            }
        } else if (orientation == 1) {
            while (currentSelection.gridY != 0 && (!onWhite || !passedBlack)) {

                if (currentSelection.gridY != 0) {
                    currentSelection = puzzle[currentSelection.gridY - 1][currentSelection.gridX];
                } else {
                    if (!currentSelection.isBlack())
                        passedBlack = true;
                }

                if (!currentSelection.isBlack()) {
                    onWhite = true;
                    if (!nextClue) {
                        passedBlack = true;
                    }
                    safeSquare = currentSelection;
                } else if (currentSelection.isBlack()) {
                    passingBlack = true;
                }
            }
        }

        currentSelection = safeSquare;
        parent.select(currentSelection);
    }

    public Square nextWhite(int x, int y) {
        int xPos = x;
        int yPos = y;
        Square currentSquare = puzzle[yPos][xPos];
        while (currentSquare.isBlack()) {
            xPos += 1;
            if (xPos == width) {
                yPos += 1;
                xPos = 0;
            }
            if (yPos == height) {
                javax.swing.JOptionPane.showMessageDialog(null, "There were are no whites squares in the puzzle...this won't be very interesting");
                return (puzzle[yPos][xPos]);
            }
            currentSquare = puzzle[yPos][xPos];
        }
        return currentSquare;
    }

    public Square previousClue(int xPos, int yPos) {
        return nextClue(xPos, yPos, false);
    }

    public Square nextClue(int xPos, int yPos) {
        return nextClue(xPos, yPos, true);
    }

    public Square nextClue(int xPos, int yPos, boolean forward) {
        int x = xPos;
        int y = yPos;

        do {
            x += (forward ? 1 : -1);
            if (x == (forward ? width : -1)) {
                y += (forward ? 1 : -1);
                x = (forward ? 0 : width - 1);
                if (y == (forward ? height : -1))
                    return puzzle[yPos][xPos];
            }
        } while (puzzle[y][x].number <= 0);

        return (puzzle[y][x]);
    }

    public void select(Square s, int orientation) {
        Iterator<Square> iter = minorSelected.iterator();
        Square current;
        while (iter.hasNext()) {
            current = iter.next();
            current.removeSelection();
        }
        minorSelected.clear();

        s.setSelection(1);
        minorSelected.add(s);

        int x = s.gridX, y = s.gridY;
        boolean notBlack = true;
        if (orientation == 0) {
            int xRight = x + 1, xLeft = x - 1;
            while (xRight < width && notBlack) {
                if (puzzle[y][xRight].isBlack()) {
                    notBlack = false;
                } else {
                    puzzle[y][xRight].setSelection(0);
                    minorSelected.add(puzzle[y][xRight]);
                    xRight++;
                }
            }

            notBlack = true;

            while (xLeft >= 0 && notBlack) {
                if (puzzle[y][xLeft].isBlack()) {
                    notBlack = false;
                } else {
                    puzzle[y][xLeft].setSelection(0);
                    minorSelected.add(puzzle[y][xLeft]);
                    xLeft--;
                }
            }
        } else {
            int yAbove = y - 1, yBelow = y + 1;
            while (yBelow < height && notBlack) {
                if (puzzle[yBelow][x].isBlack()) {
                    notBlack = false;
                } else {
                    puzzle[yBelow][x].setSelection(0);
                    minorSelected.add(puzzle[yBelow][x]);
                    yBelow++;
                }
            }

            notBlack = true;

            while (yAbove >= 0 && notBlack) {
                if (puzzle[yAbove][x].isBlack()) {
                    notBlack = false;
                } else {
                    puzzle[yAbove][x].setSelection(0);
                    minorSelected.add(puzzle[yAbove][x]);
                    yAbove--;
                }
            }
        }
    }
}

