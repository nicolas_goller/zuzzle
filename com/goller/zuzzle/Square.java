package goller.zuzzle;

import javax.swing.*;
import java.awt.*;

public class Square extends JLabel {
    private static final long serialVersionUID = 1L;
    // string otherwise char is fine...for tricky crosswords that use than one letter in a square
    public String input, answer;
    int gridX, gridY;
    int number, offset;

    // for a loaded puzzle either to solve or to continue writing/editing
    public Square(int black, String input, String answer, int gridX, int gridY) {
        setFocusTraversalKeysEnabled(false);
        number = black;
        this.input = input;
        this.answer = answer;
        setup();
        this.gridX = gridX;
        this.gridY = gridY;
    }

    // for puzzle creation
    public Square(int gridX, int gridY) {
        this(0, "", "", gridX, gridY);
    }

    public Square() {
        this(0, "", "", 0, 0);
    }

    public boolean isBlack() {
        return number == -1 ? true : false;
    }

    public void toggleBlack() {
        if (number >= 0) {
            number = -1;
            setBackground(Controller.darkBackground);
            setBorder(Controller.blackline);
        } else if (number == -1) {
            number = 0;
            setBorder(Controller.blackline);
            setBackground(Controller.normalBackground);
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        //g.setFont(Controller.numberFont);
        Dimension d = getSize();
        offset = (int) (d.height / 3f);
        g.setFont(new Font("Arial", Font.PLAIN, offset));
        if (number > 0)
            g.drawString("" + number, 2, offset + 4);
    }

    public void setup() {
        setMinimumSize(new Dimension(30, 30));
        setPreferredSize(new Dimension(30, 30));
        setAlignmentX(CENTER_ALIGNMENT);
        setAlignmentY(CENTER_ALIGNMENT);
        setBorder(Controller.blackline);
        setBackground(number == 0 ? Color.WHITE : Color.BLACK);
        setVerticalAlignment(BOTTOM);
        setHorizontalAlignment(CENTER);
        setFont(Controller.mainFont);
        setOpaque(true);
    }

    public void setSelection(int degree) {
        setBorder(degree == 0 ? Controller.selectedMinor : Controller.selectedMajor);
        setBackground(degree == 0 ? Controller.selectedCompatriots : Controller.selectedBackground);
    }

    public void removeSelection() {
        setBorder(Controller.blackline);
        if (!isBlack()) {
            setBackground(Controller.normalBackground);
        } else {
            setBackground(Controller.darkBackground);
        }
    }

    public void redrawString() {
        input(input);
    }

    public void input(String string) {
        input = string.toUpperCase();
        Controller.mainFont = new Font("Arial", Font.BOLD, (int) (offset * (3 / 2d)));
        setFont(Controller.mainFont);
        setText(input);
    }
}
