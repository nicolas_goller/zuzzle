package goller.zuzzle;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ClueList implements ListSelectionListener {
    private JList<JTextField> list;
    private DefaultListModel<JTextField> model;

    public ClueList(String[] entries, CrosswordPanel parent) {
        JTextField[] textFields;
        model = new DefaultListModel<JTextField>();
        for (int i = 0; i < entries.length; i++) {
            JTextField temp = new JTextField(entries[i]);
            model.addElement(temp);
        }

        list = new JList<JTextField>(model);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);
        list.addListSelectionListener(this);
        list.setVisibleRowCount(5);
        JScrollPane listScrollPane = new JScrollPane(list);
    }

    public ClueList(CrosswordPanel parent) {
        this(new String[0], parent);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }
}  
